`timescale 1ns / 1ps

module conv_1(
	input CLK,								// �rajel
	input START,							// bemeneti enged�lyez� jel (k�p mem�ri�ba t�lt�se k�sz)
	input [7:0] KernelInputData,		// sz�r�: 5x5*8bit
	output [4:0] KernelIndex,			// sz�r� �rt�kei
	input [12:0] StartAddressOfOutput,		// RAM_1 �ltalunk haszn�lhat� tartom�ny�nak kezd�c�me	
	input [7:0] InputData,				// RAM_0-b�l �rkez� k�p pixel
	output [7:0] OutputData,			// RAM_1-be �rt k�p pixel
	output [12:0] InputAddress,			// RAM_0 c�mz�se
	output [12:0] OutputAddress,			// RAM_1 c�mz�se
	output WriteEnableConv,				// RAM_1 �r�s enged�lyez� bit
	output ReadyConv						// konvol�ci� v�g�t jelz� bit
   );
// ---------------------------------------------------------------------
//											PARAM�TEREK:
// ---------------------------------------------------------------------
	parameter size_in_power = 0;		// RAM_1 c�mz�se
	parameter dim_image = 0;			
	parameter dim_kernel = 5;			// 5*5-�s filter
	parameter dim_conv = dim_image - dim_kernel + 1;
	
	reg prev_start = 0;	
	reg full = 1;
	reg [7:0] out_tmp;
	
	reg [12:0] sum_kernel;
	reg [7:0] kernel [24:0];
	reg [10:0] i;	// kis m�trix oszlopa
	reg [10:0] j;	// kis m�trix sora
	reg [10:0] X;	// kis m�trix orig�-oszlopa
	reg [10:0] Y;	// kis m�trix orig�-sora
	reg [4:0] k;
	reg [20:0] temp;
	reg ready;
	
	wire [7:0] act_func_in;
	wire [7:0] act_func_out;
	
Aktivation_Function Aktivation_Function(
	.address(act_func_in),
	.data(act_func_out)
    );
	
// v�rakoz�s
always @ (posedge CLK)
begin
	if((START > prev_start) && full)	// en_in felfut��l�t v�rjuk
	begin
		full <= 0;
		sum_kernel <= KernelInputData;
		kernel[24] <= KernelInputData;
		k <= 23;
	end
	else if(full)
	begin
		k <= 24;
		i <= 0;	
		j <= 0;	
		X <= 0;	
		Y <= 0;	
		out_tmp <= 0;
		temp <= 0;
		ready <= 0;
	end
	prev_start <= START;
end

// kernel �sszeg�nek kisz�m�t�sa �s kernel bet�lt�se
always @ (posedge CLK)
begin
	if(!full)
	begin
		if(k != 31)						// t�lcsordul�s
		begin
		sum_kernel <= sum_kernel + KernelInputData;
		kernel[k] <= KernelInputData;
		k <= k - 1;
		end
	end
end
assign KernelIndex = k;

// 2D - konvol�ci�
always @ (posedge CLK)
begin
	if(!full)
	begin
		if((dim_conv * Y + X) < dim_conv * dim_conv)
		begin
			temp <= temp + InputData*kernel[24 - (i + j*dim_kernel)];
			if(!((j == dim_kernel-1) && (i == dim_kernel-1)))
			begin
				if(i < dim_kernel - 1)
				begin
					i <= i + 1;	
				end
				else
				begin
					i <= 0;
					j <= j + 1;
				end
				if((j == 0) && (i == 0))
				begin
					temp <= InputData*kernel[24];
					out_tmp <= act_func_out;
				end
			end
			else
			begin
				j <= 0;
				i <= 0;
				out_tmp <= act_func_out;
				if(X == dim_image-dim_kernel)
				begin
					Y <= Y + 1;
					X <= 0;
				end
				else
					X <= X + 1;
			end
		end
		else	// ha v�gign�zt�k a teljes k�pet
		begin
			full <= 1;
			ready <= 1;
		end
	end
end
assign InputAddress = i + j*dim_image + X + Y*dim_image;

assign OutputData = out_tmp;
assign OutputAddress = StartAddressOfOutput + dim_conv * Y + X;	
assign WriteEnableConv = ~full;
assign ReadyConv = ready;
assign act_func_in = temp/sum_kernel;

endmodule
