`timescale 1ns / 1ps

module Layer3(
	input CLK,
	input RST,
	input ReadyLayer2,
	output [11:0] InputAddress,
	input [7:0] InputData,
	output [7:0] OutputData,
	output ReadyLayer3
    );
// ---------------------------------------------------------------------
//											PARAM�TEREK:
// ---------------------------------------------------------------------
parameter kernel_num = 0;						// filterek sz�ma
parameter RAM_2_size_in_power = 0;			// RAM_2 c�mz�se	
parameter dim_image = 0;						// Bemeneti k�p m�rete
parameter dim_conv = dim_image-4;			// Konvol�ci� m�rete
parameter neuron_num = 5;
		
defparam Conv.size_in_power = RAM_2_size_in_power;			
defparam Conv.dim_image = dim_image;
// ---------------------------------------------------------------------
// 									 DEKLAR�CI�K:
// ---------------------------------------------------------------------
wire [RAM_2_size_in_power - 1:0] Layer2_addr;				// RAM_1 olvas�s�hoz
reg Layer2_rdy_edge = 1;											// Layer1 k�szenl�t�t jelzi
reg [2:0] state = 0;													//	state == 0 			->			Layer2-re v�runk
																			//	state == 1 			->			konvol�ci�
																			//	state == 2 			->			
																			// state == 3			->			egy �tem ready jelz�sre
// CONV
wire rdy_CONV;															// konvol�ci� v�g�t jelzi
wire write_en_CONV;													// RAM_2 �r�s�hoz enged�lyez� jel
reg start_CONV = 0;													// konvol�ci� kezdet�t jelzi
reg conv_state = 0;													// konvol�ci� �llapotai: 0-> �j kernel; 1->k�sz
reg [RAM_2_size_in_power - 1:0] start_address;				// RAM_2 �rand� tartom�ny�nak kezd�c�me(konvol�ci�nk�nt v�ltozik)
reg [7:0] ConvolutionOutputMemory [kernel_num*3 - 1:0];
// NEURONOK
reg [7:0] OutputNeuron;
reg [30:0] tmp;
reg [kernel_num - 1 :0] NeuronInputCntr;
reg [7:0] OutputNeuronMemory [neuron_num - 1 :0];

reg [7:0] weights [kernel_num*neuron_num*3 + neuron_num - 1:0];	// az �sszes neuronhoz tartoz� s�lyt tartalmazza

wire [7:0] ConvolutionOutputData;
reg [7:0] KernelMemory [25*3-1:0];					// a r�teg kerneleit itt t�roljuk
wire [4:0] KernelNumberForConv;
wire [7:0] kernel;
reg [10:0] i = 0;
reg [30:0] sum_kernel = 0;
reg [8:0] cntr;										 	// Ciklikus konvol�ci�k sz�ml�l�ja

wire [7:0] act_func_in;
wire [7:0] act_func_out;
	
Aktivation_Function Aktivation_Function(
	.address(act_func_in),
	.data(act_func_out)
    );
// ---------------------------------------------------------------------
// 										P�LD�NYOS�T�S
// ---------------------------------------------------------------------	
//						  Konvol�ci�s modul p�ld�nyos�t�sa:
// ---------------------------------------------------------------------
conv_2 Conv(
	.CLK(CLK),				
	.START(start_CONV),			
	.KernelInputData(kernel),	
	.KernelIndex(KernelNumberForConv),
	.StartAddressOfOutput(start_address),
	.InputAddress(Layer2_addr),
	.InputData(InputData),
	.OutputData(ConvolutionOutputData),					
	.OutputAddress(),
	.WriteEnableConv(write_en_CONV),
	.ReadyConv(rdy_CONV)			
	);
	
always @ (posedge CLK)
begin
	if(i <75)
	begin
		KernelMemory[i] <= i+1;
		i <= i + 1;
	end
end
assign kernel = KernelMemory[KernelNumberForConv + (cntr/kernel_num)*25];
// ---------------------------------------------------------------------
// 							 PROCEDUR�LIS BLOKKOK:
// ---------------------------------------------------------------------
always @ (posedge CLK)
begin
	if(!RST)
	begin
		if((state == 0) && (ReadyLayer2 > Layer2_rdy_edge))// STATE = 0
		begin
			state <= 1;
			cntr <= 0;
		end
		else
			Layer2_rdy_edge <= ReadyLayer2;						// Layer2 ready jel�nek felfut� �l�t keress�k
		if(state == 1)													// STATE = 1
		begin
			if(cntr < kernel_num*3)
			begin
				if(conv_state == 0)
				begin 
					start_address <= cntr;	
					start_CONV <= 1;
					conv_state <= 1;
				end
				else
				begin
					start_CONV <= 0;
					if(rdy_CONV)
					begin
						cntr <= cntr + 1;
						conv_state <= 0;
						ConvolutionOutputMemory[cntr] <= ConvolutionOutputData;
					end
				end
			end
			else
			begin
				start_CONV <= 0;
				cntr <= 0;
				NeuronInputCntr <= 0;
				tmp <= 0;
				sum_kernel <= 0;
				state <= 2;
			end
		end
		if(state == 2)														// STATE = 2
		begin
			if(cntr < neuron_num)
			begin
				if(NeuronInputCntr < kernel_num*3)
				begin
					sum_kernel <= sum_kernel + weights[NeuronInputCntr + cntr*kernel_num];
					tmp <= tmp + ConvolutionOutputMemory[NeuronInputCntr]*weights[NeuronInputCntr + cntr*kernel_num];
					NeuronInputCntr <= NeuronInputCntr + 1;
				end
				else
				begin
					tmp <= 0;
					cntr <= cntr + 1;
					NeuronInputCntr <= 0;
					OutputNeuronMemory[cntr] <= act_func_out;
					sum_kernel <= 0;
				end
			end
			else
			begin
				state <= 3;
				NeuronInputCntr <= 0;
				tmp <= 0;
				sum_kernel <= 0;
				cntr <= 0;
			end
		end
		if(state == 3)														// STATE = 3
		begin
			if(NeuronInputCntr < neuron_num)
			begin
				sum_kernel <= sum_kernel + weights[kernel_num*neuron_num + NeuronInputCntr];
				tmp <= tmp + OutputNeuronMemory[NeuronInputCntr]* weights[kernel_num*neuron_num + NeuronInputCntr];
				NeuronInputCntr <= NeuronInputCntr + 1;
			end
			else
			begin
				OutputNeuron <= act_func_out;
				state <= 4;
			end
		end
		if(state == 4)														// STATE = 4
		begin
			state <= 0;
		end
	end
	else
	begin
		cntr <= 0;
		start_CONV <= 0;
		conv_state <= 0;
	end
end	

assign InputAddress = Layer2_addr;									// RAM_1 c�mz�se olvas�sra
assign ReadyLayer3 = (state == 4);									// Layer2 m�veleteinek befejez�s�t jelzi
assign OutputData = OutputNeuron;
assign act_func_in = tmp/sum_kernel;
// ---------------------------------------------------------------------
//									        S�LYOK:
// ---------------------------------------------------------------------
reg [9:0] j = 0;
always@(posedge CLK)
begin
	if(j < (kernel_num*neuron_num*3 + neuron_num))
	begin
		weights[j] <= j;
		j <= j + 1;
	end
end
endmodule
