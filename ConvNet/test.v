`timescale 1ns / 1ps

module test;

	// Inputs
	reg RST;
	reg CLK;

	// Instantiate the Unit Under Test (UUT)
	CNN uut (
		.RST(RST),
		.CLK(CLK)
	);

	initial begin
		// Initialize Inputs
		CLK = 0; 
		RST = 1;

		// Wait 10250 ns for global reset and memory initializaton
		#10250;
      RST <= 0;
		// Add stimulus here

	end
   always #5 CLK <= ~CLK;
endmodule

