`timescale 1ns / 1ps

module RAM_1(
	input CLK,								// �rajel
	//	Be- �s kimenetek a konvol�ci�s modulok sz�m�ra
	input [12:0] address_R_wr,		
	input [12:0] address_G_wr,
	input [12:0] address_B_wr,
	input [7:0] din_R,					// 
	input [7:0] din_G,
	input [7:0] din_B,
	input write_enable_CONV,			// konvol�ci� �ltali �r�st enged�lyez� bit
	//	Be- �s kimenetek a pooling modul sz�m�ra
	input [12:0] address_rd,			// pooling �rja/olvassa a mem�ri�t ezzel a c�mmel
	input [7:0] din_pool,				// pooling �rja a mem�ri�t
	output [7:0] dout_pool,				// pooling olvassa a mem�ri�t
	input write_enable_POOL,			// pooling �r�s enged�lyez� bitje
	// Layer2 sz�m�ra
	input [10:0] Layer2_address,
	output [7:0] Layer2_data
   );
// ---------------------------------------------------------------------
//											PARAM�TEREK:
// ---------------------------------------------------------------------	
	parameter feature_num = 0;		//h�ny darab 28*28-as k�p
	parameter size_in_power = 0;
	 
	reg [7:0] memory [feature_num - 1:0];
	always @(posedge CLK) 
	begin
        if (write_enable_CONV)  				// a mem�ri�t a konvol�ci�s modulok �rj�k
		  begin	
		  // a "-1" a c�mekben az �rajelre �temezett mintav�tel okozta cs�sz�s miatt van
            memory[address_R_wr - 1] <= din_R;
				memory[address_G_wr - 1] <= din_G;
				memory[address_B_wr - 1] <= din_B;
        end
		  if (write_enable_POOL)				// a mem�ri�t a pooling modul �rja	
				memory[address_rd] <= din_pool;
   end	 

assign dout_pool = memory[address_rd];
assign Layer2_data = memory[Layer2_address];
endmodule