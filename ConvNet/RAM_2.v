`timescale 1ns / 1ps

module RAM_2(
	input CLK,								// �rajel
	//	Bemenetek a konvol�ci�s modulok sz�m�ra
	input [11:0] address_CONV,		
	input [7:0] din_CONV,					// 
	input write_enable_CONV,			// konvol�ci� �ltali �r�st enged�lyez� bit
	//	Be- �s kimenetek a pooling modul sz�m�ra
	input [11:0] address_POOL,			// pooling �rja/olvassa a mem�ri�t ezzel a c�mmel
	input [7:0] din_POOL,				// pooling �rja a mem�ri�t
	output [7:0] dout_POOL,				// pooling olvassa a mem�ri�t
	input write_enable_POOL,			// pooling �r�s enged�lyez� bitje
	// Layer3 sz�m�ra
	input [11:0] Layer3_address,
	output [7:0] Layer3_data
   );
// ---------------------------------------------------------------------
//											PARAM�TEREK:
// ---------------------------------------------------------------------	
	parameter feature_num = 0;		//h�ny darab 10*10-es k�p
	parameter size_in_power = 0;
	 
	reg [7:0] memory [feature_num - 1:0];
	always @(posedge CLK) 
	begin
        if (write_enable_CONV)  				// a mem�ri�t a konvol�ci�s modulok �rj�k
		  begin	
		  // a "-1" a c�mekben az �rajelre �temezett mintav�tel okozta cs�sz�s miatt van
            memory[address_CONV - 1] <= din_CONV;
        end
		  if (write_enable_POOL)				// a mem�ri�t a pooling modul �rja	
				memory[address_POOL] <= din_POOL;
   end	 

assign dout_POOL = memory[address_POOL];
assign Layer3_data = memory[Layer3_address];
endmodule