`timescale 1ns / 1ps

// 2x2-es pooling

module pool_1(
	input CLK,						// �rajel
	input [7:0] din,				// RAM_1-b�l �rkez� adat
	input START,					// konvol�ci� k�szenl�t�t jelz� bit
	output [12:0] address,		// RAM_1-et c�mz� vonal (R/W eset�n is)
	output [7:0] dout,			// RAM_1-be �rand� adat
	output write_en,				// RAM_1 �r�s�t enged�lyez� bit
	output STOP						// POOLING v�g�t jelz� bit
    );
// ---------------------------------------------------------------------
//											PARAM�TEREK:
// ---------------------------------------------------------------------
parameter size_in_power = 0;			// RAM c�mz�se
parameter size = 0;						// bemeneti k�p nagys�g
parameter kernel_num = 0;

reg full = 1;
reg [7:0] tmp;
reg [9:0] i;
reg [9:0] j;
reg [2:0] k;
reg [size_in_power - 3:0] cim;			// a eredm�ny ki�r�sa, negyed akkora k�p -> 2 bittel r�videbb c�m el�g
reg [size_in_power - 1:0] addr;

// v�rakoz�s
always @ (posedge CLK)
begin
	if(START)
		full <= 0;
	if(full != 0)
	begin
		i <= 0;	// oszlop
		j <= 0;	// sor
		k <= 0;	// 4 �tem
		addr <= 10'bx;
		cim <= -1;
	end
end


always @ (posedge CLK)
begin
	if(!full)
	begin
		if((j < size*kernel_num*3 - 1) || (cim < (kernel_num*3*(size/2)*(size/2))))
		begin
			if(i < size - 1)
			begin
				case(k)
					0	:	addr <= i + size*j;
					1	:	addr <= i + 1 + size*j;
					2	:	addr <= i + size + size*j;
					3	:	addr <= i + size + 1 + size*j;
					4	:	addr <= cim;
					default	:	k <= k;
				endcase
				k <= k + 1;
				if(k == 3)
				begin
					if(i < size - 2)
					begin
						i <= i + 2;
					end
					else
					begin
						i <= 0;
						j <= j + 2; 
					end
				end
			end
		end
		else	// ha v�gign�zt�k a teljes k�pet
		begin
			full <= 1;
		end
	end
end
always @ (posedge CLK)
begin
	if(k == 1)
	begin
		tmp <= din;
		cim <= cim + 1;
	end
	else
	begin
		if(din > tmp)
			begin
				tmp <= din;
			end
	end
end

assign address = addr;
assign write_en = (k==6);
assign dout = (k == 5) ? tmp : dout;
assign STOP = full && !START;

endmodule