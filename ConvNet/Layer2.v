`timescale 1ns / 1ps

module Layer2(
	input RST,
	input CLK,
	// Layer1-hez tartoz� vezet�kek
	output [11:0] InputAddress,
	input [7:0] InputData,
	input ReadyLayer1,
	//	Layer_3_hoz tartoz� vezet�kek
	input [11:0] OutputAddress,
	output [7:0] OutputData,
	output ReadyLayer2
    );

// ---------------------------------------------------------------------
//											PARAM�TEREK:
// ---------------------------------------------------------------------
parameter kernel_num = 0;						// filterek sz�ma
parameter RAM_2_size_in_power = 0;			// RAM_2 c�mz�se	
parameter dim_image = 0;						// Bemeneti k�p m�rete
parameter dim_conv = dim_image-4;			// Konvol�ci� m�rete


defparam RAM_2.feature_num = kernel_num*3*dim_conv*dim_conv;
defparam RAM_2.size_in_power = RAM_2_size_in_power;			
defparam Conv.size_in_power = RAM_2_size_in_power;			
defparam Conv.dim_image = dim_image;
defparam Pool.size_in_power = RAM_2_size_in_power;
defparam Pool.kernel_num = kernel_num;
defparam Pool.size = dim_conv;
// ---------------------------------------------------------------------
// 									 DEKLAR�CI�K:
// ---------------------------------------------------------------------
wire [RAM_2_size_in_power - 1:0] Layer1_addr;				// RAM_1 olvas�s�hoz
reg Layer1_rdy_edge = 1;											// Layer1 k�szenl�t�t jelzi
reg [1:0] state = 0;													//	state == 0 			->			Layer1-re v�runk
																			//	state == 1 			->			konvol�ci�
																			//	state == 2 			->			pooling
																			// state == 3			->			egy �tem ready jelz�sre
wire [RAM_2_size_in_power - 1:0] RAM_2_address_wr;			// RAM_2 c�m busz �r�sra
wire [7:0] RAM_2_din;												// RAM_2 bemeneti busz
// CONV
wire rdy_CONV;															// konvol�ci� v�g�t jelzi
wire write_en_CONV;													// RAM_2 �r�s�hoz enged�lyez� jel
reg start_CONV = 0;													// konvol�ci� kezdet�t jelzi
reg conv_state = 0;													// konvol�ci� �llapotai: 0-> �j kernel; 1->k�sz
reg [RAM_2_size_in_power - 1:0] start_address;				// RAM_2 �rand� tartom�ny�nak kezd�c�me(konvol�ci�nk�nt v�ltozik)

// POOL
wire rdy_POOL;															// pooling v�g�t jelzi
wire write_en_POOL;													// RAM_2 �r�s�hoz enged�lyez� jel
reg start_POOL = 0;													// pooling kezdet�t jelzi
wire [7:0] data_read;												// RAM_2 olvas�sa
wire [7:0] data_write;												// RAM_2 �r�sa
wire [RAM_2_size_in_power - 1:0] address_pool;				// RAM_2 c�mz�se (�r�s �S olvas�s)

// Layer3
wire [7:0] L3_data;													// Layer3 sz�m�ra �rkez� adat RAM_2-b�l 

reg [7:0] KernelMemory [25*3-1:0];								// a r�teg kerneleit itt t�roljuk
wire [4:0] KernelNumberForConv;
wire [7:0] kernel;
reg [10:0] i = 0;
reg [8:0] cntr;														// Ciklikus konvol�ci�k sz�ml�l�ja
// ---------------------------------------------------------------------
// 										P�LD�NYOS�T�S
// ---------------------------------------------------------------------	
// 						M�sodik feature mem�ria p�ld�nyos�t�sa: 
// ---------------------------------------------------------------------			
RAM_2 RAM_2(
	.CLK(CLK),							
	.address_CONV(RAM_2_address_wr),		
	.din_CONV(RAM_2_din),					 
	.write_enable_CONV(write_en_CONV),		
	.address_POOL(address_pool),
	.din_POOL(data_write),			
	.dout_POOL(data_read),			
	.write_enable_POOL(write_en_POOL),	
	.Layer3_address(OutputAddress),
	.Layer3_data(L3_data)
   );	
// ---------------------------------------------------------------------
//						  Konvol�ci�s modul p�ld�nyos�t�sa:
// ---------------------------------------------------------------------
conv_2 Conv(
	.CLK(CLK),				
	.START(start_CONV),			
	.KernelInputData(kernel),	
	.KernelIndex(KernelNumberForConv),
	.StartAddressOfOutput(start_address),
	.InputAddress(Layer1_addr),
	.InputData(InputData),
	.OutputData(RAM_2_din),					
	.OutputAddress(RAM_2_address_wr),
	.WriteEnableConv(write_en_CONV),
	.ReadyConv(rdy_CONV)			
	);

always @ (posedge CLK)
begin
	if(i <75)
	begin
		KernelMemory[i] <= i+1;
		i <= i + 1;
	end
end
assign kernel = KernelMemory[KernelNumberForConv + (cntr/kernel_num)*25];
// ---------------------------------------------------------------------
// 						(MAX) Pooling modul p�ld�nyos�t�sa:
// ---------------------------------------------------------------------

pool_1 Pool(
	.CLK(CLK),
	.din(data_read),
	.START(start_POOL),
	.address(address_pool),
	.dout(data_write),
	.write_en(write_en_POOL),
	.STOP(rdy_POOL)
   );
	 
// ---------------------------------------------------------------------
// 							 PROCEDUR�LIS BLOKKOK:
// ---------------------------------------------------------------------
always @ (posedge CLK)
begin
	if(!RST)
	begin
		if((state == 0) && (ReadyLayer1 > Layer1_rdy_edge)) // STATE = 0
		begin
			state <= 1;
		end
		else
			Layer1_rdy_edge <= ReadyLayer1;						// Layer1 ready jel�nek felfut� �l�t keress�k
		if(state == 1)													// STATE = 1
		begin
			if(cntr < kernel_num*3)
			begin
				if(conv_state == 0)
				begin 
					start_address <= cntr*dim_conv*dim_conv;	
					start_CONV <= 1;
					conv_state <= 1;
				end
				else
				begin
					start_CONV <= 0;
					if(rdy_CONV)
					begin
						cntr <= cntr + 1;
						conv_state <= 0;
					end
				end
			end
			else
			begin
				start_CONV <= 0;
				start_POOL <= 1;
				cntr <= 0;
				state <= 2;
			end
		end
		if(state == 2)														// STATE = 2
		begin
			start_POOL <= 0;
			if(rdy_POOL == 1)
			begin
				state <= 3;
				Layer1_rdy_edge <= 1;
			end
		end
		if(state == 3)														// STATE = 3
			state <= 0;
	end
	else
	begin
		cntr <= 0;
		start_CONV <= 0;
		start_POOL <= 0;
		conv_state <= 0;
	end
end	

assign InputAddress = Layer1_addr;									// RAM_1 c�mz�se olvas�sra
assign ReadyLayer2 = (state == 3);									// Layer2 m�veleteinek befejez�s�t jelzi
assign OutputData = L3_data;											// adat Layer3 sz�m�ra

endmodule
