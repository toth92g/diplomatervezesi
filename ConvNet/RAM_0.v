`timescale 1ns / 1ps

module RAM_0(
	input [9:0] address_wr,
	input [9:0] address_rd,
	input write_enable,
	input CLK,		
	input [7:0] din,
	output [7:0] dout	
    );
	
	reg [7:0] memory [1024 - 1:0];
	always @(posedge CLK) 
	begin
        if (write_enable) 
		  begin
            memory[address_wr] <= din;
        end
   end	 
	
assign dout = memory[address_rd];
endmodule