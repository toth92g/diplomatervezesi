`timescale 1ns / 1ps

module Layer1(
	input RST,
	input CLK,
	input Init,
	// RAM_0-hoz tartoz� vezet�kek				// bej�v� k�pek
	output [9:0] InputAddress_Red,				// 1kByteot kell c�mezni
	input [7:0] InputData_Red,
	output [9:0] InputAddress_Green,				// 1kByteot kell c�mezni
	input [7:0] InputData_Green,
	output [9:0] InputAddress_Blue,				// 1kByteot kell c�mezni
	input [7:0] InputData_Blue,
	// Layer2-h�z tartoz� vezet�kek
	input [10:0] OutputAddress,		// negyede -> -2bit
	output [7:0] OutputData,
	output ReadyLayer1
	);
// ---------------------------------------------------------------------
//											PARAM�TEREK:
// ---------------------------------------------------------------------
parameter kernel_num = 0;						// filterek sz�ma
parameter RAM_1_size_in_power = 0;			// RAM_1 c�mz�se	
parameter dim_image = 0;						// Bemeneti k�p nagys�g
parameter dim_conv = dim_image-4;


defparam RAM_1.feature_num = kernel_num*3*(dim_image - 4)*(dim_image - 4);
defparam RAM_1.size_in_power = RAM_1_size_in_power;			
defparam Conv_1_R.size_in_power = RAM_1_size_in_power;			
defparam Conv_1_G.size_in_power = RAM_1_size_in_power;
defparam Conv_1_B.size_in_power = RAM_1_size_in_power;
defparam Conv_1_R.dim_image = dim_image;
defparam Conv_1_G.dim_image = dim_image;
defparam Conv_1_B.dim_image = dim_image;
defparam Pool.size_in_power = RAM_1_size_in_power;
defparam Pool.kernel_num = kernel_num;
defparam Pool.size = dim_image - 4;
// ---------------------------------------------------------------------
// 									 DEKLAR�CI�K:
// ---------------------------------------------------------------------
// RAM_0 olvas�sa
wire [9:0] RAM_0_address_rd [2:0];								// RAM_0 c�m busz olvas�sra
// RAM_1 vezet�kei
wire [RAM_1_size_in_power - 1:0] RAM_1_address_wr [2:0];	// RAM_1 c�m �r�sra
wire [7:0] RAM_1_din [2:0];										// RAM_1 �r�sra

reg [1:0] state = 3;													//	state == 0 			->			inicializ�l�s
																			//	state == 1 			->			konvol�ci�
																			//	state == 2 			->			pooling
																			//	state == 3 			->			inicializ�l�sra v�r
// CONV	
wire rdy_CONV;															// konvol�ci� v�g�t jelzi
wire write_en_CONV;													// RAM_1 �r�s�hoz enged�lyez� jel
reg start_CONV = 0;													// konvol�ci� kezdet�t jelzi
reg conv_state = 0;													// konvol�ci� �llapotai: 0-> �j kernel; 1->k�sz
reg [RAM_1_size_in_power - 1:0] start_address;				// RAM_1 �rand� tartom�ny�nak kezd�c�me(konvol�ci�nk�nt v�ltozik)

// POOL
wire rdy_POOL;															// pooling v�g�t jelzi
wire write_en_POOL;													// RAM_1 �r�s�hoz enged�lyez� jel
reg start_POOL = 0;													// pooling kezdet�t jelzi
wire [7:0] data_read;												// RAM_1 olvas�sa
wire [7:0] data_write;												// RAM_1 �r�sa
wire [RAM_1_size_in_power - 1:0] address_pool;				// RAM_1 c�mz�se (�r�s �S olvas�s)

// Layer2
wire [7:0] L2_data; 													// RAM_1 kimen� adatai Layer2 sz�m�ra

reg [7:0] KernelMemory [25*kernel_num-1:0];					// a r�teg kerneleit itt t�roljuk
wire [4:0] KernelNumberForConv;
wire [7:0] kernel;
reg [10:0] i = 0;
reg [8:0] cntr;														// Ciklikus konvol�ci�k sz�ml�l�ja
// ---------------------------------------------------------------------
// 											Mem�ria
// ---------------------------------------------------------------------				
RAM_1 RAM_1(
	.address_R_wr(RAM_1_address_wr[0] + 0*dim_conv*dim_conv),
	.address_G_wr(RAM_1_address_wr[1] + 1*dim_conv*dim_conv),
	.address_B_wr(RAM_1_address_wr[2] + 2*dim_conv*dim_conv),
	.address_rd(address_pool),
	.write_enable_CONV(write_en_CONV),			// el�g, mert �gyis egyszerre adnak jelet a konvol�ci�k
	.write_enable_POOL(write_en_POOL),
	.CLK(CLK),		
	.din_R(RAM_1_din[0]),
	.din_G(RAM_1_din[1]),
	.din_B(RAM_1_din[2]),
	.din_pool(data_write),
	.dout_pool(data_read),
	.Layer2_address(OutputAddress),
	.Layer2_data(L2_data)
    );		
// ---------------------------------------------------------------------
//											Konvol�ci�k
// ---------------------------------------------------------------------
conv_1 Conv_1_R(
	.CLK(CLK),				
	.START(start_CONV),			
	.KernelInputData(kernel),	
	.KernelIndex(KernelNumberForConv),
	.StartAddressOfOutput(start_address),
	.InputAddress(RAM_0_address_rd[0]),
	.InputData(InputData_Red),
	.OutputData(RAM_1_din[0]),					
	.OutputAddress(RAM_1_address_wr[0]),
	.WriteEnableConv(write_en_CONV),
	.ReadyConv(rdy_CONV)				
	);
conv_1 Conv_1_G(
	.CLK(CLK),				
	.START(start_CONV),			
	.KernelInputData(kernel),
	.KernelIndex(),
	.StartAddressOfOutput(start_address),				
	.InputAddress(RAM_0_address_rd[1]),
	.InputData(InputData_Green),
	.OutputData(RAM_1_din[1]),					
	.OutputAddress(RAM_1_address_wr[1]),
	.WriteEnableConv(),
	.ReadyConv()
	);
conv_1 Conv_1_B(
	.CLK(CLK),				
	.START(start_CONV),			
	.KernelInputData(kernel),
	.KernelIndex(),
	.StartAddressOfOutput(start_address),
	.InputAddress(RAM_0_address_rd[2]),
	.InputData(InputData_Blue),
	.OutputData(RAM_1_din[2]),					
	.OutputAddress(RAM_1_address_wr[2]),
	.WriteEnableConv(),
	.ReadyConv()
	);

always @ (posedge CLK)
begin
	if(RST)
	begin
		KernelMemory[i] <= i+1;
		i <= i + 1;
	end
end
assign kernel = KernelMemory[KernelNumberForConv + cntr*25];
// ---------------------------------------------------------------------
// 								(MAX) Pooling:
// ---------------------------------------------------------------------
pool_1 Pool(
	.CLK(CLK),
	.din(data_read),
	.START(start_POOL),
	.address(address_pool),
	.dout(data_write),
	.write_en(write_en_POOL),
	.STOP(rdy_POOL)
    );
// ---------------------------------------------------------------------
// 							 PROCEDUR�LIS BLOKKOK
// ---------------------------------------------------------------------
always @ (posedge CLK)
begin
	if(!RST)
	begin
		if((state == 0) && !Init)									// STATE = 0
			state <= 1;
		if(state == 1)													// STATE = 1
		begin
			if(cntr < kernel_num)
			begin
				if(conv_state == 0)
				begin	
					start_address <= cntr*3*dim_conv*dim_conv;	
					start_CONV <= 1;
					conv_state <= 1;
				end
				else
				begin
					start_CONV <= 0;
					if(rdy_CONV)
					begin
						cntr <= cntr + 1;
						conv_state <= 0;
					end
				end
			end
			else
			begin
				start_CONV <= 0;
				start_POOL <= 1;
				cntr <= 0;
				state <= 2;
			end
		end
		if(state == 2)														// STATE = 2
		begin
			start_POOL <= 0;
			if(rdy_POOL)
			begin
				state <= 3;
			end
		end
		if((state == 3) && Init)										// STATE = 3
		begin
			state <= 0;
		end
	end
	else
	begin
		cntr <= 0;
		start_CONV <= 0;
		start_POOL <= 0;
		conv_state <= 0;
	end
end			

assign InputAddress_Red = RAM_0_address_rd[0];					// RAM_0 olvas�sa
assign InputAddress_Green = RAM_0_address_rd[1];				// RAM_0 olvas�sa
assign InputAddress_Blue = RAM_0_address_rd[2];					// RAM_0 olvas�sa

assign ReadyLayer1 = (state == 3);									// Layer1 m�veleteinek befejez�s�t jelzi
assign OutputData = L2_data;											// Layer2 ezen a vezetk�n kaphatja meg az eredm�nyeket

endmodule